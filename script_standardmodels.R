rm(list=ls())
options(digits = 6)
#setwd('C:/Users/ducom/Documents/AA Thesis') 

#.libPaths('C:/Users/ducom/AppData/Local/Temp/RtmpGSYXL8/downloaded_packages')
#.libPaths("C:/Program Files/R/R-4.0.3/library" )
.libPaths("C:/Users/ducom/Documents/R/win-library/4.0")
#.libPaths("V:/UserData/061337/Rlib")
install.packages("foreign")
install.packages("nnet")
install.packages("ggplot2")
install.packages("reshape2")
install.packages("dplyr")
install.packages("MASS")
install.packages("Hmisc")
install.packages("glmpathcr")
install.packages("VGAM")
install.packages("xlsx")
install.packages("tidyverse")
install.packages("pROC")

library(foreign)
library(nnet)
library(ggplot2)
library(reshape2)
library(dplyr)
library(MASS)
library(Hmisc)
library(glmpathcr)
library(VGAM)
library(stats4)
library(splines)
library(xlsx)
library(tidyverse)
library(pROC)

categories = "four_v2"
#train_size = 0.7
train_size = 0.6
sample_zero_mn = 1
sample_zero_ordered= 1

datasets <- read_data(categories, sample_zero_mn, sample_zero_ordered, trainsize)
train_mn <- datasets[[1]]
test_mn <- datasets[[2]]
train_ordered <- datasets[[3]]
test_ordered <- datasets[[4]]

############# ORDERED LOGIT
thisformula <- outcome ~ age + male + r1_hb10 + r1_hb20 + r1_hb30 + r1_hb40 + r1_hb47+ r2_hb10 + r2_hb20 + r2_hb30 + r2_hb40 + r2_hb47
ordered.fit <- polr(formula= thisformula, data = train_ordered, Hess=TRUE)
prob_pred <- predict(ordered.fit, newdata=train_ordered, type="prob")
prob_pred <- as.data.frame(prob_pred)
y <- as.data.frame(train_ordered$outcome2)
names(y)[1]<- "outcome2"

############## CONTINUATION LOGIT 
thisformula <- outcome ~ age + male + r1_hb10 + r1_hb20 + r1_hb30 + r1_hb40 + r1_hb47+ r2_hb10 + r2_hb20 + r2_hb30 + r2_hb40 + r2_hb47
fit.cr2 <- vglm(formula = thisformula, family=cratio(parallel=TRUE ~ 0), data=train_ordered)
prob_pred <- cr_probtable(fit.cr2, train_ordered, cat_nr = "four_v2")
y <- as.data.frame(train_ordered$outcome2)
names(y)[1]<- "outcome2"

########## FUNCTIONS
read_data <- function(categories, sample_zero_mn, sample_zero_ordered, trainsize){
  
  # if(categories=="three"){
  #   setwd('C:/Users/ducom/Documents/AA Thesis/data_file/clean_data/three_categories')
  #   cat_levels <- c("NO_LESIONS", "SMALL_LESION", "AN")
  # } else if (categories=="four"){
  #   setwd('C:/Users/ducom/Documents/AA Thesis/data_file/clean_data/four_categories')
  #   cat_levels <- c("NO_COLO", "NO_ADENOMA", "NON_ADVANCED", "AA_CRC")
  # } else if (categories=="four_v2"){
  #   setwd('C:/Users/ducom/Documents/AA Thesis/data_file/clean_data/four_categories_v2')
  #   cat_levels <- c("NO_COLO", "COLO_NONADVANCED", "AA", "CRC")
  # } else if (categories=="five"){
  #   setwd('C:/Users/ducom/Documents/AA Thesis/data_file/clean_data/five_categories')
  #   cat_levels <-  c("NO_COLO", "COLO_NOLESIONS", "NAA", "AA", "CRC")
  # } else if (categories=="six"){
  #   setwd('C:/Users/ducom/Documents/AA Thesis/data_file/clean_data/all_categories')
  #   cat_levels <-  c("NO_COLO", "NOLESIONS", "HYPERPLASTIC", "NONADVANCED", "ADVANCED", "CANCER")
  # } else{
  #   print('Please select a number of categories')
  # }
  
  
  if(categories=="three"){
    setwd('C:/Users/ducom/Documents/AA Thesis/data_file/clean_data/three_categories')
    cat_levels <- c("NO_LESIONS", "SMALL_LESION", "AN")
  } else if (categories=="four"){
    setwd('V:/UserData/061337/data_file/clean_data/four_categories')
    cat_levels <- c("NO_COLO", "NO_ADENOMA", "NON_ADVANCED", "AA_CRC")
  } else if (categories=="four_v2"){
    setwd('V:/UserData/061337/data_file/clean_data/four_categories_v2')
    cat_levels <- c("NO_COLO", "COLO_NONADVANCED", "AA", "CRC")
  } else if (categories=="five"){
    setwd('V:/UserData/061337/data_file/clean_data/five_categories')
    cat_levels <-  c("NO_COLO", "COLO_NOLESIONS", "NAA", "AA", "CRC")
  } else if (categories=="six"){
    setwd('V:/UserData/061337/data_file/clean_data/all_categories')
    cat_levels <-  c("NO_COLO", "NOLESIONS", "HYPERPLASTIC", "NONADVANCED", "ADVANCED", "CANCER")
  } else{
    print('Please select a number of categories')
  }
  
  sample_multinomial <- read.csv("sample_multinomial.csv", sep=",")
  sample_ordered <- read.csv("sample_ordered.csv", sep=",")
  uniques <- unique(sample_multinomial$outcome)
  ref_cat <- uniques[which.max(tabulate(match(sample_multinomial$outcome, uniques)))]
  table(sample_multinomial$outcome)
  
  sample_multinomial$outcome <- as.factor(sample_multinomial$outcome)
  sample_multinomial$outcome <- relevel(sample_multinomial$outcome, ref=ref_cat)
  unique(sample_ordered$outcome)
  
  sample_ordered$outcome <- ordered(sample_ordered$outcome, levels=cat_levels)
  
  train_mn <- sample_frac(sample_multinomial, train_size)
  sample_id <- as.numeric(rownames(train_mn))
  test_mn <- sample_multinomial[-sample_id,]
  rm(sample_id)
  
  train_ordered <- sample_frac(sample_ordered, train_size)
  sample_id <- as.numeric(rownames(train_ordered))
  test_ordered <- sample_ordered[-sample_id,]
  rm(sample_id)
  
  ############# UNDERSAMPLE ZERO OBSERVATIONS #####
  zeros_obs <- train_mn[train_mn$outcome == ref_cat, ]
  nonzeros_obs <- train_mn[train_mn$outcome != ref_cat, ]
  keep_obs <- round(sample_zero_mn*(sum(train_mn$outcome==ref_cat)))
  keep_zeros <- zeros_obs[sample(1:nrow(zeros_obs), keep_obs, replace=FALSE),]
  train_mn <- rbind(nonzeros_obs, keep_zeros)
  train_mn <- train_mn[sample(nrow(train_mn)),] 
  rm(zeros_obs, nonzeros_obs, keep_obs, keep_zeros)
  
  zeros_obs <- train_ordered[train_ordered$outcome == ref_cat, ]
  nonzeros_obs <- train_ordered[train_ordered$outcome != ref_cat, ]
  keep_obs <- round(sample_zero_ordered*(sum(train_ordered$outcome==ref_cat)))
  keep_zeros <- zeros_obs[sample(1:nrow(zeros_obs), keep_obs, replace=FALSE),]
  train_ordered <- rbind(nonzeros_obs, keep_zeros)
  train_ordered <- train_ordered[sample(nrow(train_ordered)),]
  rm(zeros_obs, nonzeros_obs, keep_obs, keep_zeros)
  
  for (i in 1:length(cat_levels)){
    train_ordered$outcome2[train_ordered$outcome==cat_levels[i]] <- i-1
  }
  
  for (i in 1:length(cat_levels)){
    test_ordered$outcome2[test_ordered$outcome==cat_levels[i]] <- i-1
  }
  
  for (i in 1:length(cat_levels)){
    train_mn$outcome2[train_mn$outcome==cat_levels[i]] <- i-1
  }
  
  for (i in 1:length(cat_levels)){
    test_mn$outcome2[test_mn$outcome==cat_levels[i]] <- i-1
  }
  
  return(list(train_mn, test_mn, train_ordered, test_ordered))
}

cr_probtable <- function(fit.cr2, pred_dataset, cat_nr){
  predictions_cr <- predict(fit.cr2, untransform=TRUE)
  predictions_cr <- predict(fit.cr2, untransform=TRUE, newdata = pred_dataset)
  
  if (cat_nr=="four"){
    predictions_cr <- cbind(predictions_cr, NO_COLO=c(1-predictions_cr[,1]))
    predictions_cr <- cbind(predictions_cr, NO_ADENOMA=c(predictions_cr[,1]*(1-predictions_cr[,2])))
    predictions_cr <- cbind(predictions_cr, NON_ADVANCED=c(predictions_cr[,1]*predictions_cr[,2]*(1-predictions_cr[,3])))
    predictions_cr <- cbind(predictions_cr, AA_CRC=c(1-predictions_cr[,c('NO_COLO')]-predictions_cr[,c('NO_ADENOMA')]-predictions_cr[,c('NON_ADVANCED')] ))
    prob_pred <- subset(predictions_cr, select = c("NO_COLO","NO_ADENOMA", "NON_ADVANCED", "AA_CRC"))
  }else if(cat_nr=="three"){
    predictions_cr <- cbind(predictions_cr, NO_COLO=c(1-predictions_cr[,1]))
    predictions_cr <- cbind(predictions_cr, SMALL_LESION=c(predictions_cr[,1]*(1-predictions_cr[,2])))
    predictions_cr <- cbind(predictions_cr, AN=c(1-predictions_cr[,c('NO_LESIONS')]-predictions_cr[,c('SMALL_LESION')]))
    prob_pred <- subset(predictions_cr, select = c("NO_COLO", "SMALL_LESION", "AN"))
  }else if (cat_nr=="four_v2"){
    predictions_cr <- cbind(predictions_cr, NO_COLO=c(1-predictions_cr[,1]))
    predictions_cr <- cbind(predictions_cr, COLO_NONADVANCED=c(predictions_cr[,1]*(1-predictions_cr[,2])))
    predictions_cr <- cbind(predictions_cr, AA=c(predictions_cr[,1]*predictions_cr[,2]*(1-predictions_cr[,3])))
    predictions_cr <- cbind(predictions_cr, CRC=c(1-predictions_cr[,c('NO_COLO')]-predictions_cr[,c('COLO_NONADVANCED')]-predictions_cr[,c('AA')] ))
    prob_pred <- subset(predictions_cr, select = c("NO_COLO", "COLO_NONADVANCED", "AA", "CRC"))
  }else if (cat_nr=="five"){
    predictions_cr <- cbind(predictions_cr, NO_COLO=c(1-predictions_cr[,1]))
    predictions_cr <- cbind(predictions_cr, COLO_NOLESIONS=c(predictions_cr[,1]*(1-predictions_cr[,2])))
    predictions_cr <- cbind(predictions_cr, NAA=c(predictions_cr[,1]*predictions_cr[,2]*(1-predictions_cr[,3])))
    predictions_cr <- cbind(predictions_cr, AA=c(predictions_cr[,1]*predictions_cr[,2]*predictions_cr[,3]*(1-predictions_cr[,4])))
    predictions_cr <- cbind(predictions_cr, CRC=c(1-predictions_cr[,c('NO_COLO')]-predictions_cr[,c('COLO_NOLESIONS')]-predictions_cr[,c('NAA')]-predictions_cr[,c('AA')] ))
    prob_pred <- subset(predictions_cr, select = c("NO_COLO","COLO_NOLESIONS" , "NAA", "AA", "CRC"))
  }else{
    predictions_cr <- cbind(predictions_cr, NO_COLO=c(1-predictions_cr[,1]))
    predictions_cr <- cbind(predictions_cr, NO_LESIONS=c(predictions_cr[,1] * (1-predictions_cr[,2])))
    predictions_cr <- cbind(predictions_cr, HYPERPLASTIC=c(predictions_cr[,1] * predictions_cr[,2] * (1-predictions_cr[,3])))
    predictions_cr <- cbind(predictions_cr, NONADVANCED=c(predictions_cr[,1] * predictions_cr[,2] * predictions_cr[,3] * (1-predictions_cr[,4])))
    predictions_cr <- cbind(predictions_cr, ADVANCED=c(predictions_cr[,1] * predictions_cr[,2] * predictions_cr[,3] * predictions_cr[,4]* (1-predictions_cr[,5])))
    predictions_cr <- cbind(predictions_cr, CANCER = c(1-predictions_cr[,c('NO_COLO')]-predictions_cr[,c('NO_LESIONS')]-predictions_cr[,c('HYPERPLASTIC')]-predictions_cr[,c('NONADVANCED')]-predictions_cr[,c('ADVANCED')]))
  }
  return(prob_pred)
}


######################################################
############### MULTINOMIAL LOGIT ####################
######################################################
# https://datasciencebeginners.com/2018/12/20/multinomial-logistic-regression-using-r/

multinom.fit <- multinom(outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood, data = train_mn)
#multinom.fit <- multinom(outcome ~ age + male + r1_hb10  + r1_hb20  + r1_hb30  + r1_hb40  + r1_hb47 + r2_hb10  + r2_hb20  + r2_hb30  + r2_hb40  + r2_hb47, data = train_mn)
multinom.fit <- multinom(outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta + r2_hbdelta2, data = train_mn)
summary(multinom.fit)
exp(coef(multinom.fit)) #odds
head(probability.table <-fitted(multinom.fit))
#rm(probability.table)

train_mn$predicted <- predict(multinom.fit, newdata = train_mn, "class")
check <- train_mn[train_mn$predicted != ref_cat, ]
ctable_mn <- table(train_mn$outcome, train_mn$predicted)
ctable_mn
mn_accuracy <- round((sum(diag(ctable_mn))/sum(ctable_mn))*100,2)

sum(test_mn$outcome != ref_cat)
test_mn$predicted <- predict(multinom.fit, newdata = test_mn, "class")
checktest <- test_mn[test_mn$predicted != ref_cat, ]
ctabletest_mn  <- table(test_mn$outcome, test_mn$predicted)
ctabletest_mn
mn_testaccuracy <- round((sum(diag(ctabletest_mn))/sum(ctabletest_mn))*100,2)


############ multi ROC #############################
prob_pred <- predict(multinom.fit, newdata=test_mn, type="prob")
head(prob_pred)
multiclass.roc(test_mn$outcome, prob_pred)


#######################################################
rm(mn_accuracy, mn_testaccuracy, check, checktest)
rm(ctable_mn, ctable_test_mn, probability.table, multinom.fit)

######################################################
#################ORDERED LOGIT########################
######################################################

ordered.fit <- polr(formula= outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood, data = train_ordered, Hess=TRUE)
#ordered.fit <- polr(formula = outcome ~ age + male + r1_hb10  + r1_hb20  + r1_hb30  + r1_hb40  + r1_hb47 + r2_hb10  + r2_hb20  + r2_hb30  + r2_hb40  + r2_hb47, data = train_ordered, Hess=TRUE)
ordered.fit <- polr(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta + r2_hbdelta2, data = train_ordered, Hess=TRUE)
ordered.fit <- polr(formula = outcome ~ age + male + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta, data = train_ordered, Hess=TRUE )
ordered.fit <- polr(formula = outcome ~ age + male + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta + dum_noblood, data=train_ordered, Hess=TRUE)
summary(ordered.fit)
coef_OL <- coef(summary(ordered.fit))
p <- pnorm(abs(coef_OL[,"t value"]), lower.tail=FALSE)*2
coef_OL <- cbind(coef_OL, "p value" =p)
ci <- confint(ordered.fit)
exp(coef(ordered.fit))
exp(cbind(OR=coef(ordered.fit),ci)) # OR and CI
head(probability.table_ordered <-fitted(ordered.fit))

########## multiclass ROC #############
prob_pred <- predict(ordered.fit, newdata=test_ordered, type="prob")
head(prob_pred)
multiclass.roc(test_ordered$outcome, prob_pred)

########################################################
#########
############# DIT DEEL NOG AANPASSEN
####################################################
###################################################
probability.table_ordered <- predict(ordered.fit, newdata=test_ordered, type="prob")

threshold_lesions = 0.7
probability.table_ordered <- as.data.frame(probability.table_ordered)
#probability.table_ordered$outcome <- train_ordered$outcome
probability.table_ordered$outcome <- test_ordered$outcome 

check <- probability.table_ordered[probability.table_ordered$NO_COLO < threshold_lesions, ]
check2 <- check[check$outcome != "NO_COLO", ]
check3 <- check2[check2$outcome != "NO_ADENOMA", ]
check3 <- check2[check2$outcome != "COLO_NONADVANCED",]
rm(check, check2, check3)

if (three_cat ==TRUE){
  probability.table_ordered$pred = with(probability.table_ordered, 
                                        ifelse(probability.table_ordered$NO_LESIONS<threshold_lesions 
                                               ,ifelse(probability.table_ordered$SMALL_LESION < probability.table_ordered$AN, 'AN', 'SMALL_LESION')
                                               ,'NO_LESION'))
  probability.table_ordered$pred <- ordered(probability.table_ordered, levels=c("NO_LESION", "SMALL_LESION", "AN"))
  check_OL1 <- probability.table_ordered[probability.table_ordered$NO_LESIONS < threshold_lesions, ]
  
}else if (four_cat==TRUE){
  probability.table_ordered <- probability.table_ordered %>% mutate(pred = case_when(
    NO_COLO < threshold_lesions & NO_ADENOMA >= NON_ADVANCED & NO_ADENOMA > AA_CRC ~ "NO_ADENOMA",
    NO_COLO < threshold_lesions & AA_CRC > NON_ADVANCED & AA_CRC >= NO_ADENOMA ~ "AA_CRC",
    NO_COLO < threshold_lesions & NON_ADVANCED >= NO_ADENOMA & NON_ADVANCED > AA_CRC ~ "NON_ADVANCED",
    NO_COLO > threshold_lesions ~ "NO_COLO"
  ))
  probability.table_ordered$pred <- ordered(probability.table_ordered$pred, levels=c("NO_COLO", "NO_ADENOMA", "NON_ADVANCED", "AA_CRC"))
  check_OL1 <- probability.table_ordered[probability.table_ordered$NO_LESIONS < threshold_lesions, ]
} else if (four_cat_v2==TRUE){
  probability.table_ordered <- probability.table_ordered %>% mutate(pred = case_when(
    NO_COLO <= threshold_lesions & COLO_NONADVANCED >= AA & COLO_NONADVANCED > CRC ~ "COLO_NONADVANCED",
    NO_COLO <= threshold_lesions & AA > COLO_NONADVANCED & AA >= CRC ~ "AA",
    NO_COLO <= threshold_lesions & CRC >= COLO_NONADVANCED & CRC > AA ~ "CRC",
    NO_COLO > threshold_lesions ~ "NO_COLO"
  ))
  probability.table_ordered$pred <- ordered(probability.table_ordered$pred, levels=c("NO_COLO", "COLO_NONADVANCED", "AA", "CRC"))
  check_OL2 <- probability.table_ordered[probability.table_ordered$NO_LESIONS < threshold_lesions, ]
} 
ctable_OL1 <- table(probability.table_ordered$outcome, probability.table_ordered$pred)
ctable_OL1

########################################################
####################################################
###################################################

train_ordered$predicted <- predict(ordered.fit, newdata = train_ordered, "class")
unique(train_ordered$predicted)
check_OL <- train_ordered[train_ordered$predicted != ref_cat, ]
ctable_OL <- table(train_ordered$outcome, train_ordered$predicted)
ctable_OL
accuracy_OL <- round((sum(diag(ctable_OL))/sum(ctable_OL))*100,2)

test_ordered$predicted <- predict(ordered.fit, newdata = test_ordered, "class")
checktest_OL <- test_ordered[test_ordered$predicted != ref_cat, ]
ctabletest_OL <- table(test_ordered$outcome, test_ordered$predicted)
ctabletest_OL
accuracytest_OL <- round((sum(diag(ctabletest_OL))/sum(ctabletest_OL))*100,2)

rm(check_OL, checktest_OL)
rm(coef_OL, ordered.fit, probability.table_ordered, accuracy_OL, accuracytest_OL, ctable_OL, ctabletest_OL, p)

######################################################
###############CONTINUATIO RATIO MODEL################
######################################################
fit.cr2 <- vglm(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood, family=cratio(parallel=TRUE ~ 0), data=train_ordered)
#fit.cr2 <- vglm(formula =  outcome ~ age + male + r1_hb10  + r1_hb20  + r1_hb30  + r1_hb40  + r1_hb47 + r2_hb10  + r2_hb20  + r2_hb30  + r2_hb40  + r2_hb47, family=cratio(parallel=TRUE~0) ,data=train_ordered)
fit.cr2 <- vglm(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood + r1_hbvalue2 + r2_hbvalue2, family=cratio(parallel=TRUE~0), data=train_ordered)
fit.cr2 <- vglm(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta2, family=cratio(parallel=TRUE~0), data=train_ordered)
fit.cr2 <- vglm(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood + r1_hbvalue2 + r2_hbvalue2, family=cratio(parallel=TRUE~0), data=train_ordered)
fit.cr2 <- vglm(formula = outcome ~ age + male + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta + dum_noblood, family=cratio(parallel=TRUE~0), data=train_ordered)
fit.cr2 <- vglm(formula = outcome ~ age + male + r1_hbvalue2 + r2_hbvalue2, family=cratio(parallel=TRUE~0), data=train_ordered)
summary(fit.cr2)
exp(coef(fit.cr2))
predictions_cr <- predict(fit.cr2, untransform=TRUE)
predictions_cr <- predict(fit.cr2, untransform=TRUE, newdata = test_ordered)

########## multiclass ROC #############
prob_pred <- cr_probtable(fit.cr2, test_ordered, cat_nr = "four_v2")
prob_pred_cr <-prob_pred
head(prob_pred)
multiclass.roc(test_ordered$outcome, prob_pred)
########################

predictions_cr <- as.data.frame(predictions_cr)
#risk_lesion <- mean(predictions_cr$NON_ADVANCED) +  mean(predictions_cr$AA_CRC)
#threshold_lesions <- 1-10*risk_lesion
threshold_lesions <- 0.6
#predictions_cr$outcome <- train_ordered$outcome
predictions_cr$outcome <- test_ordered$outcome 

if (three_cat == TRUE){
  #predictions_cr$pred = with(predictions_cr, 
  #      ifelse(predictions_cr$NO_LESIONS<threshold_lesions 
  #              ,ifelse(2*predictions_cr$SMALL_LESION < predictions_cr$AN, 'AN', 'SMALL_LESION')
  #              ,'NO_LESION'))
  predictions_cr <- predictions_cr %>% mutate(pred = case_when(
    NO_LESIONS < threshold_lesions & SMALL_LESION >= AN  ~ "SMALL_LESION",
    NO_LESIONS < threshold_lesions & AN >= SMALL_LESION  ~ "AN",
    NO_LESIONS > threshold_lesions ~ "NO_LESIONS"
  ))
  predictions_cr$pred <- ordered(predictions_cr$pred, levels=c("NO_LESIONS", "SMALL_LESION", "AN"))
  check_CR2 <- predictions_cr[predictions_cr$NO_LESIONS < threshold_lesions, ]
} else if (four_cat == TRUE){
  predictions_cr <- predictions_cr %>% mutate(pred = case_when(
    NO_COLO < threshold_lesions & NO_ADENOMA >= NON_ADVANCED & NO_ADENOMA > AA_CRC ~ "NO_ADENOMA",
    NO_COLO < threshold_lesions & NON_ADVANCED >= NO_ADENOMA & NON_ADVANCED > AA_CRC ~ "NON_ADVANCED",
    NO_COLO < threshold_lesions & AA_CRC > NO_ADENOMA & AA_CRC >= NO_ADENOMA ~ "AA_CRC",
    NO_COLO > threshold_lesions ~ "NO_COLO"
  ))
  check_CR2 <- predictions_cr[predictions_cr$NO_COLO < threshold_lesions, ]
  predictions_cr$pred <- ordered(predictions_cr$pred, levels=c("NO_COLO", "NO_ADENOMA", "NON_ADVANCED", "AA_CRC"))
}else if (four_cat_v2 == TRUE){
  predictions_cr <- predictions_cr %>% mutate(pred = case_when(
    NO_COLO <= threshold_lesions &  COLO_NONADVANCED >= AA &  COLO_NONADVANCED > CRC ~ " COLO_NONADVANCED",
    NO_COLO <= threshold_lesions & AA >= COLO_NONADVANCED & AA >= CRC ~ "AA",
    NO_COLO <= threshold_lesions & CRC >  COLO_NONADVANCED & CRC >= AA ~ "CRC",
    NO_COLO > threshold_lesions ~ "NO_COLO"
  ))
  check_CR2 <- predictions_cr[predictions_cr$NO_COLO < threshold_lesions, ]
  predictions_cr$pred <- ordered(predictions_cr$pred, levels=c("NO_COLO", " COLO_NONADVANCED", "AA", "CRC"))
}

ctable_CR <- table(predictions_cr$outcome, predictions_cr$pred)
ctable_CR


###### Calibration plots, basecase #####
# Deel populatie op in 100 groepen naar risicoscore
# Per groep, hoeveel laesies gedetecteerd
# (performance metric)

r <- 100 # number of categories
m <- data.frame(matrix(NA, nrow=r, ncol=4, dimnames=list(rep(paste0("q",1:r)), c("Real_CRC_rate","Real_AA_rate","Mean_risk_score","pred"))))
f=2
hltest <- rep(NA,2)
brier <- rep(NA,2)

multinom.fit <- multinom(outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood + r1_hbvalue2 + r2_hbvalue2, data = train_mn)
multinom.fit <- multinom(outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood, data = train_mn)
#test_ordered_reserve <- test_ordered
test_ordered <- test_mn

ordered.fit <- polr(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2 + dum_noblood, data=train_ordered, Hess=TRUE)
ordered.fit <- polr(formula = outcome ~ age + male + r1_hbvalue + r2_hbvalue + r2_hbdelta + dum_noblood, data=train_ordered, Hess=TRUE)
ordered.fit <- polr(formula = outcome ~ age + male + r1_hbvalue2 + r2_hbvalue2 + r2_hbdelta + dum_noblood, data=train_ordered, Hess=TRUE)


prob_pred <- predict(ordered.fit, newdata=test_ordered, type="prob")
prob_pred <- predict(multinom.fit, newdata=test_ordered, type="prob")
prob_pred <- prob_pred_cr

#######let op dit is dus met hele set
prob_pred <- predict(ordered.fit, newdata = train_ordered, type="prob")

prob_pred <- as.data.frame(prob_pred)
weightAA = 1
weightCRC = 1
weightNA = 0 ################# TESTEN MET SOMMATIE 
#prob_pred$risk_score <- (weightAA*prob_pred$AA + weightCRC*prob_pred$CRC + weightNA*prob_pred$COLO_NONADVANCED)/(weightAA+weightCRC+weightNA)
prob_pred$risk_score <- (weightAA*prob_pred$AA + weightCRC*prob_pred$CRC + weightNA*prob_pred$COLO_NONADVANCED)
#prob_pred$risk_score <- (weightAA*prob_pred$AA + weightCRC*prob_pred$CRC + weightNA*prob_pred$COLO_NONADVANCED)

#prob_pred$outcome <- test_ordered$outcome  
prob_pred$outcome <- train_ordered$outcome
ordsample <- prob_pred[order(prob_pred$risk_score),]

#ordsample$r <- c(rep(1:r, each=round(nrow(test_ordered)/r)))[1:nrow(test_ordered)]
ordsample$r <- c(rep(1:r, each=round(nrow(train_ordered)/r)))[1:nrow(train_ordered)]
ordsample[is.na(ordsample)] <- 100

for (q in 1:r) {
  index <- (ordsample$r==q)
  #m[offset+q,2] <- mean(ordsample[index,"truth"])
  subsample <- ordsample[index, ]
  
  m[q,1] <- sum(subsample$outcome == "CRC")/nrow(subsample)
  m[q,2] <- sum(subsample$outcome == "AA" | subsample$outcome == "CRC")/nrow(subsample)
  m[q,3] <- mean(subsample$risk_score)
}

m$rr_crc <- NA
m$rr_aa <- NA
#m$rr_crc<- m$Real_CRC_rate/mean(test_ordered$outcome=="CRC")
#m$rr_aa <- m$Real_AA_rate/(mean(test_ordered$outcome=="CRC")+mean(test_ordered$outcome=="AA"))
m$rr_crc<- m$Real_CRC_rate/mean(train_ordered$outcome=="CRC")
m$rr_aa <- m$Real_AA_rate/(mean(train_ordered$outcome=="CRC")+mean(train_ordered$outcome=="AA"))

ggplot(data=m) +
  #ggplot(data=subset(m, outcome=="Cancer")) +
  #geom_point(aes(x=Mean_risk_score*100, y=Real_CRC_rate*100), size=2.5, color="blue") +
  geom_point(aes(x=Mean_risk_score*100, y=Real_AA_rate*100), size=2.5, color="blue") +
  #geom_errorbar(aes(x=pred*100, ymin=ci_lb*100, ymax=ci_ub*100), color="blue") +
  geom_abline(intercept=0, slope=1, linetype="dashed", color="grey40") +
  #geom_smooth(aes(x=Mean_risk_score*100, y=Real_CRC_rate*100),method='loess') +
  geom_smooth(aes(x=Mean_risk_score*100, y=Real_AA_rate*100),method='loess') +
  #geom_hline(yintercept=1, linetype="dashed", color="black") +
  # Q10: geom_text(data=test, aes(x=c(0.002,0.002),y=c(0.06,0.06),label=label), hjust = 0) +
  #geom_text(data=test, aes(x=c(0.002,0.002),y=c(15,15),label=label), hjust = 0) + #Q100
  #facet_grid(~outcome, scales = "free") +
  #scale_color_gradient(low=brewer.pal(n=9, name="Reds")[4], high=brewer.pal(n=9, name="Reds")[9]) +
  #scale_color_gradientn(colours = rev(brewer.pal(n=11, name="RdYlGn"))) +
  #scale_y_continuous(breaks=c(0,2,4,6,8,10,12,14), limits=c(0,14)) +
  guides(color=F) +
  ylab("Observed AA/CRC detection rate, %") +
  xlab("Assigned Risk Score") +
  theme_bw() +
  theme(axis.title=element_text(size=14),
        axis.text=element_text(size=14),
        strip.text.x = element_text(size = 14))
# save with 750x340 dimensions for manuscript

threshold_riskscore = 2.5
above_riskthreshold <- ordsample[ordsample$risk_score > threshold_riskscore/100,]
nrow(above_riskthreshold)/nrow(test_ordered)
highrisk_people <- test_ordered[rownames(above_riskthreshold),]

full_pred <- predict(ordered.fit, newdata=sample_ordered, type="prob")
full_pred <- as.data.frame(full_pred)
full_pred$risk_score <- (weightAA*full_pred$AA + weightCRC*full_pred$CRC)/(weightAA+weightCRC)
full_pred$outcome <- sample_ordered$outcome  
ordsample <- full_pred[order(full_pred$risk_score),] 
above_riskthreshold <- ordsample[ordsample$risk_score > threshold_riskscore/100,]
nrow(above_riskthreshold)/nrow(test_ordered)
highrisk_people <- sample_ordered[rownames(above_riskthreshold),]

mean(highrisk_people$r1_hbvalue)
mean(highrisk_people$r2_hbvalue)
mean(sample_ordered$r1_hbvalue)
mean(sample_ordered$r2_hbvalue)

mean(highrisk_people$r2_IC)
mean(sample_ordered$r2_IC)
mean(highrisk_people$r2_IC)/mean(sample_ordered$r2_IC)

sum(highrisk_people$r3_IC)
sum(sample_ordered$r3_IC)

check1<-sample_ordered[sample_ordered$r3_IC==T,]
check <- sample_ordered[sample_ordered$r2_IC==T,]

mean(highrisk_people$male)
mean(sample_ordered$male)

mean(highrisk_people$age)
mean(sample_ordered$age)

############# ROC ###########################

t <- data.frame(matrix(NA, ncol=5, dimnames=list(1,c("model","form","auc_mu","auc_lb","auc_ub"))))
r <- 100

sample_ordered$AdvNeo <- with(sample_ordered, ifelse(sample_ordered$outcome=="CRC" | sample_ordered$outcome == "AA", TRUE, FALSE))
#sample_ordered$AdvNeo <- with(sample_ordered, ifelse(sample_ordered$outcome=="AA_CRC", TRUE, FALSE))
truth <- sample_ordered$AdvNeo

for (model in c("OL", "CR")){
  j <- ifelse(model=="OL", 0, 1)
  
  form1 <- as.formula("outcome ~ 1 + age + male + r1_hbvalue + r2_hbvalue")
  form2 <- as.formula("outcome ~ 1 + age + male + r1_hbvalue2 + r2_hbvalue2 + dum_noblood")
  form3 <- as.formula("outcome ~ 1 + age + male + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  form4 <- as.formula("outcome ~ 1 + age + male + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2 +  dum_noblood")
  
  form5 <- as.formula("outcome ~ 1 + age + male + r1_hb10 + r1_hb20 + r1_hb30 + r1_hb40 + r1_hb47 + r2_hb10 + r2_hb20 + r2_hb30 + r2_hb40 + r2_hb47")
  
  form6 <- as.formula("outcome ~ 1 + age + age*r1_hbvalue + male + r1_hbvalue + r2_hbvalue")
  form7 <- as.formula("outcome ~ 1 + age + age*r2_hbvalue + male + r1_hbvalue + r2_hbvalue2")
  form8 <- as.formula("outcome ~ 1 + age + age*r1_hbvalue + age*r2_hbvalue + male + r1_hbvalue + r2_hbvalue")
  form9 <- as.formula("outcome ~ 1 + age + age*r1_hbvalue + male + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  form10 <- as.formula("outcome ~ 1 + age + age*r2_hbvalue + male + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  form11 <- as.formula("outcome ~ 1 + age + age*r1_hbvalue + age*r2_hbvalue + male + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  
  form12 <- as.formula("outcome ~ 1 + age + male + male*r1_hbvalue + r1_hbvalue + r2_hbvalue")
  form13 <- as.formula("outcome ~ 1 + age + male + male*r2_hbvalue + r1_hbvalue + r2_hbvalue")
  form14 <- as.formula("outcome ~ 1 + age + male + male*r1_hbvalue + male*r2_hbvalue + r1_hbvalue + r2_hbvalue")
  form15 <- as.formula("outcome ~ 1 + age + male + male*r1_hbvalue + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  form16 <- as.formula("outcome ~ 1 + age + male + male*r2_hbvalue + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  form17 <- as.formula("outcome ~ 1 + age + male + male*r1_hbvalue + male*r2_hbvalue + r1_hbvalue + r2_hbvalue + r1_hbvalue2 + r2_hbvalue2")
  
  forms=list(form1, form2, form3, form4, form5, form6, form7, form8, form9, form10, form11, form12, form13, form14, form15,form16, form17)
  
  if(model == "OL"){
    for (i in 1:length(forms)){
      print(paste0("Plotting OL model ", i))
      ordered.fit <- polr(formula=forms[[i]] , data = sample_ordered, Hess=TRUE)
      prob_pred <- predict(ordered.fit, newdata=sample_ordered, type="prob")
      prob_pred <- as.data.frame(prob_pred)
      pred_advneo <- prob_pred$AA + prob_pred$CRC
      
      t[j*length(forms)+i,1] <- paste0("model ", model)
      t[j*length(forms)+i,2] <- paste(forms[[i]][2],forms[[i]][1],forms[[i]][3])
      t[j*length(forms)+i,3:5] <- round(ci.auc(roc(truth, pred_advneo), method="bootstrap", boot.n=r),4)[c(2,1,3)]
    }
  } else if (model == "CR"){
    for (i in 1:length(forms)){
      print(paste0("Plotting CR model ", i))
      fit.cr2 <- vglm(formula = forms[[i]], family=cratio(parallel=TRUE ~ 0), data=sample_ordered)
      prob_pred <- cr_probtable(fit.cr2, sample_ordered, cat_nr=cat_nr)
      prob_pred <- as.data.frame(prob_pred)
      pred_advneo <- prob_pred$AA + prob_pred$CRC
      
      t[j*length(forms)+i,1] <- paste0("model ", model)
      t[j*length(forms)+i,2] <- paste(forms[[i]][2],forms[[i]][1],forms[[i]][3])
      t[j*length(forms)+i,3:5] <- round(ci.auc(roc(truth, pred_advneo), method="bootstrap", boot.n=r),4)[c(2,1,3)]
    }
  }
}
setwd('C:/Users/ducom/Documents/AA Thesis/data_file/output')
write.xlsx(t, file='ROCvalues_ordinal.xlsx')








