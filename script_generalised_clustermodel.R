J<- 4
C<- 3

genmodel1 <- EM2(1, y, X)
genmodel2 <- EM2(2, y, X)
genmodel3 <- EM2(3, y, X)
genmodel4 <- EM2(4, y, X)


EM <- EM2(C, y, X)
pi <- EM[[1]]
theta <- EM[[2]] 
ccp <- round(EM[[3]],2)

input_theta <- initial_theta2(J, C)
theta <- input_to_outputbetas(input_theta)
theta[[2]]
theta[[1]]


######################### FUNCTIONS ##############
genmodels <- evaluate_c(2, X, y)
genmodel1 <- models[[1]]
genmodel2 <- models[[2]]


evaluate_c2 <- function(min_C, max_C, X, y){
  
  #y <- as.data.frame(y)
  models <- list()
  for (c in min_C:max_C){
    print(paste0("Training model with cluster ",c))
    model <- EM2(c, y, X)
    print(model[[2]])
    print(model[[1]])
    #name <- paste0(c,"clusters")
    models[[c]] <- model
    #save.image(paste0("~/AA Thesis/testmap/",c,"clusters.Rdata"))
  }
  return(models)
}

initial_theta2 <- function(J, C){
  
  alphas <- sort(runif(J-1, -1, 1))
  
  for (c in 2:C){
    this_alpha <- sort(runif(J-1, -1, 1))
    alphas <- cbind(alphas, this_alpha)
  }
  alphas <- matrix(alphas, nrow=J-1, ncol=C)
  #alphas <- matrix(alphas, nrow=J-1, ncol=1)
  
  all_beta <- list()
  
  dataset <- as.data.frame(X)
  dataset$outcome <- y
  dataset$outcome <- as.factor(dataset$outcome)
  
  if (dim(X)[2]== 5){
    thisformula <- outcome ~ age + male + r1_hbvalue + r2_hbvalue + dum_noblood
    beta_OL <- cbind(-0.0126, 0.2399, 0.0375, 0.0407, -0.86)
  } else {
    thisformula <- outcome ~ age + male + r1_hb10 + r1_hb20 + r1_hb30 + r1_hb40 + r1_hb47+ r2_hb10 + r2_hb20 + r2_hb30 + r2_hb40 + r2_hb47
    ordered.fit <- polr(formula= thisformula, data = dataset, Hess=TRUE)
    beta_OL <- ordered.fit$coefficients
  }
  
  #beta_OL <- cbind(-0.0126, 0.2399, 0.0375, 0.0407, -0.86)
  #beta_OL <- cbind(-0.0126-0.5, 0.2399-0.5, 0.0375-0.5, 0.0407-0.5, -0.86-0.5)
  betas <- beta_OL
  for (c in 2:C){
    this_beta <- c()
    for (p in beta_OL){
      noise <- rnorm(1, p, 0.001)
      #noise <- runif(1, 0, 0.01)
      this_beta <- cbind(this_beta, p+noise)
    }
    betas <- rbind(betas, this_beta)
  }
  first_beta <- matrix(betas, nrow=C, ncol=dim(X)[2])
  all_beta[[1]] <- first_beta
  prev_beta <- first_beta
  next_beta <- matrix( nrow=C, ncol=dim(X)[2])
  
  for (j in 2:(J-1)){
    for (c in 1:C){
      for (l in 1:length(beta_OL)){
        #noise <- runif(1, 1, 1.5)
        max_noise <- 100* abs(beta_OL[l])
        if(max_noise > 3){
          max_noise <- 3
        }
        #noise <- runif(1, 0, max_noise)
        noise <- runif(1, -5, -max_noise)
        #print(exp(noise))
        #next_beta[c,l] <- prev_beta[c,l] * noise
        next_beta[c,l] <- noise
      }
    }
    all_beta[[j]] <- next_beta
    prev_beta <- next_beta
  }
  all_beta <- rev(all_beta)
  
  theta <- list(alphas, all_beta)
  return(theta)
}

EM2 <- function(C, y, X){
  tic("Performing EM algorithm for generalised model")
  
  W <- matrix(1/C, dim(y)[1], C)
  
  input_theta <- initial_theta2(J, C)
  M <- Mstep2(W, y, X, input_theta)
  pi <- M[[1]]
  input_theta <- M[[2]]
  
  theta <- input_to_outputbetas(input_theta)
  #theta[[1]] 
  #theta[[2]]
  
  W_new <- Estep2_new(theta, colMeans(W), X, y)
  
  ccp <- round(W_new, 2)
  iter <- 0
  diff <- 100
  
  LogLi_new <- LogL2(theta, colMeans(W), y, X)
  
  while (abs(diff) > 0.1) {
    iter <- iter + 1
    print(paste0("iteration ", iter))
    
    # prev_input_theta <- input_theta
    # prev_theta <- theta
    # prev_W <- W
    
    LogLi <- LogLi_new
    W <- W_new
    
    #print(theta)
    #print(pi)
    
    print("Performing M-step")
    M <- Mstep2(W, y, X, input_theta)
    pi <- M[[1]]
    input_theta <- M[[2]]
    
    print("Performing E-step")
    theta <- input_to_outputbetas(input_theta)
    #print(theta)
    W_new <- Estep2_new(theta, pi, X, y)
    # print((W_new))
    
    LogLi_new <- LogL2(theta, colMeans(W_new), y, X)
    
    #LogLi_here <- LogL2(theta, colMeans(W_new), y, X)
    
    # if (LogLi_here - LogLi > 0){
    #   print("Improvement made, parameters updated")
    #   LogLi_new <- LogLi_here 
    #   diff <- LogLi_new - LogLi
    # } else {
    #   input_theta <- prev_input_theta
    #   theta <- prev_theta
    #   W_new <- prev_W
    # }
    diff <- LogLi_new - LogLi
    print(diff)
  }
  pi <- M[[1]]
  theta <- input_to_outputbetas(M[[2]])
  pi
  theta
  toc()
  return(list(pi, theta, W_new))
}

objective2 <- function(data, param){
  y <- data[[1]]
  X <- data[[2]]
  W <- data[[3]]
  
  P <- dim(X)[2]
  C <- dim(W)[2]
  J <- length(param)/(C + C*P) + 1
  
  #num_alphas <- (J-1)*C
  #num_betas <- C * P
  #alphas_vec <- param[c(1:num_alphas)]
  #betas_vec <- param[c((num_alphas+1):(num_alphas+num_betas))]
  #alphas <- matrix(alphas_vec, nrow = J-1, ncol = C)
  #betas <- matrix(betas_vec, nrow = C, ncol = P)
  
  num_alphas <- (J-1)*C
  #num_alphas <- J-1
  num_betas <- C * P * (J-1)
  num_thisbeta <- num_betas/(J-1)
  
  alphas_vec <- param[c(1:num_alphas)]
  alphas <- matrix(alphas_vec, nrow = J-1, ncol = C)
  #alphas <- matrix(alphas_vec, nrow = J-1, ncol = 1)
  
  #for (i in 1:ncol(alphas)){
  #  alphas[,i] <- alphas[,1]
  #}
  
  all_betas <- list()
  for (j in 1:(J-1)){
    add <- (j-1)*num_thisbeta
    thisbetas_vec <- param[c((num_alphas+1+add):(num_alphas+num_thisbeta+add))]
    thisbetas_matrix <- matrix(thisbetas_vec, nrow = C, ncol=P)
    all_betas[[j]] <- thisbetas_matrix
  }
  
  #previous_beta <- all_betas[[1]]
  last_beta <- all_betas[[J-1]]
  #for (j in 2:(J-1)){
  for (j in (J-2):1){
    this_beta <- matrix(nrow=C, ncol=dim(X)[2])
    noise_matrix <- all_betas[[j]]
    for (c in 1:C){
      for (l in 1:dim(X)[2]){
        #this_beta[c,l] <- noise_matrix[c,l] + previous_beta[c,l]
        this_beta[c,l] <- exp(noise_matrix[c,l]) + last_beta[c,l]
      }
    }
    all_betas[[j]] <- this_beta
    #previous_beta <- this_beta 
    last_beta <- this_beta
  }
  
  #all_betas <- rev(all_betas)
  
  #print(alphas)
  #print(all_betas[[1]])
  #print(all_betas[[2]])
  #print(all_betas[[3]])
  
  cat_num <- dim(alphas)[1]+1 
  
  
  total <- 0
  for (i in 1:dim(y)[1]){
    for (j in 1:cat_num){
      if(y[i,1]==j-1){
        sum <- 0
        #for (c in 1:dim(alphas)[2]){ ####kloptdit
        for (c in 1:dim(all_betas[[1]])[1]){
          if (j==cat_num){
            first_term <- 1
          } else{
            betas <- all_betas[[j]]
            exp_term <- exp(alphas[j,c] - betas[c,]%*% X[i,])
            first_term <- exp_term/(1+exp_term)
          }
          if (j==1){
            second_term<- 0
          } else {
            prev_betas <- all_betas[[j-1]]
            exp_term2 <- exp(alphas[j-1,c] - prev_betas[c,]%*% X[i,])
            second_term <- exp_term2/(1+exp_term2)
          }
          #sum <- sum + W[i,c]*(first_term - second_term)
          # if (first_term - second_term <= 0){
          #    print(i)
          #    print(j)
          #    print(c)
          #  }
          sum <- sum + log(first_term - second_term)  #########DIT IS NIET PER DEFINITIE >0 
        }
        #total <- total + log(sum)
        total <- total + W[i,c]*sum 
      }
    }
  }
  return(-1*total)
}

LogL2 <- function(theta, pi, y, X) {
  pi <- matrix(pi)
  alphas <- theta[[1]]
  all_betas <- theta[[2]]
  
  cat_num <- dim(alphas)[1]+1 
  
  total <- 0
  for (i in 1:dim(y)[1]){
    for (j in 1:cat_num){
      #betas <- all_betas[[j]]
      if(y[i,1]==j-1){
        sum <- 0
        for (c in 1:dim(all_betas[[1]])[1]){ ######klopt dit
          if (j==cat_num){
            first_term <- 1
          } else{
            betas <- all_betas[[j]]
            exp_term <- exp(alphas[j,c] - betas[c,]%*% X[i,])
            first_term <- exp_term/(1+exp_term)
          }
          if (j==1){
            second_term<- 0
          } else {
            prev_betas <- all_betas[[j-1]]
            exp_term2 <- exp(alphas[j-1,c] - prev_betas[c,]%*% X[i,])
            second_term <- exp_term2/(1+exp_term2)
          }
          sum <- sum + pi[c]*(first_term - second_term)
        }
        total <- total + log(sum)
      }
      #prev_betas <- betas
    }
  }
  return(total)
}

Estep2_new <- function(theta, pi, X, y){
  pi <- matrix(pi)
  alphas <- theta[[1]]
  all_betas <- theta[[2]]
  ccp <- matrix(, dim(X)[1], length(pi))
  cat_num <- dim(alphas)[1]+1
  
  for(i in 1:dim(X)[1]){
    for (c in 1:length(pi)){
      numerator <- 0
      for (j in 1:cat_num){
        if (y[i]==j-1){
          if (j==cat_num){
            first_term <- 1
          } else{
            betas <- all_betas[[j]]
            exp_term <- exp(alphas[j,c] - betas[c,]%*% X[i,])
            first_term <- exp_term/(1+exp_term)
          }
          if (j==1){
            second_term<- 0
          } else {
            prev_betas <- all_betas[[j-1]]
            exp_term2 <- exp(alphas[j-1,c] - prev_betas[c,]%*% X[i,])
            second_term <- exp_term2/(1+exp_term2)
          }
          probability <- first_term - second_term
        }
      }
      numerator <- pi[c] * probability
      
      denom <- 0
      for (k in 1:length(pi)){
        for (j in 1:cat_num){
          if (y[i]==j-1){
            if (j==cat_num){
              first_term <- 1
            } else{
              betas <- all_betas[[j]]
              exp_term <- exp(alphas[j,c] - betas[k,]%*% X[i,])
              first_term <- exp_term/(1+exp_term)
            }
            if (j==1){
              second_term<- 0
            } else {
              prev_betas <- all_betas[[j-1]]
              exp_term2 <- exp(alphas[j-1,c] - prev_betas[k,]%*% X[i,])
              second_term <- exp_term2/(1+exp_term2)
            }
            probability2 <- first_term - second_term
          }
        }
        denom <- denom + pi[k] * probability2
        #print(denom)
      }
      pic <- as.numeric(numerator)/as.numeric(denom)
      ccp[i, c] <- as.numeric(pic)
    }
  }
  return(ccp)
}  

Mstep2 <- function(W, y, X, theta) {
  pi <- colMeans(W)
  data <- list(y, X, W)
  
  all_betas <- theta[[2]]
  all_betas_matrix <- c()
  for (i in all_betas){
    all_betas_matrix <- c(all_betas_matrix, i)
  }
  
  theta_vector <- c(as.vector(theta[[1]]), as.vector(all_betas_matrix))
  #print(theta_vector)
  optim <- optim(par = theta_vector, fn = objective2, data = data, method ="Nelder-Mead" ) #"BFGS""Nelder-Mead"
  theta_vector <- optim$par
  
  P <- dim(X)[2]
  C <- dim(W)[2]
  J <- length(theta_vector)/(C+C*P) + 1 #######dit klopt niet
  
  num_alphas <- (J-1)*C
  #num_alphas <- J-1
  num_betas <- C * P * (J-1)
  num_thisbeta <- num_betas/(J-1)
  
  alphas_vec <- theta_vector[c(1:num_alphas)]
  alphas <- matrix(alphas_vec, nrow = J-1, ncol = C)
  #alphas <- matrix(alphas_vec, nrow = J-1, ncol = 1)
  
  all_betas <- list()
  for (j in 1:(J-1)){
    add <- (j-1)*num_thisbeta
    thisbetas_vec <- theta_vector[c((num_alphas+1+add):(num_alphas+num_thisbeta+add))]
    thisbetas_matrix <- matrix(thisbetas_vec, nrow = C, ncol=P)
    all_betas[[j]] <- thisbetas_matrix
  }
  
  theta <- list(alphas, all_betas)
  return(list(pi, theta))
}

input_to_outputbetas <- function(theta){
  J <- dim(theta[[1]])[1] + 1
  input_betas <- theta[[2]]
  true_betas <- list()
  last_beta <- input_betas[[J-1]]
  true_betas[[J-1]] <- last_beta
  for (j in (J-2):1){
    this_beta <- matrix(nrow=C, ncol=dim(X)[2])
    addterms_matrix <- input_betas[[j]]
    for (c in 1:C){
      for (l in 1:dim(X)[2]){
        this_beta[c,l] <- exp(addterms_matrix[c,l]) + last_beta[c,l]
      }
    }
    true_betas[[j]] <- this_beta
    last_beta <- this_beta
  }
  return(list(theta[[1]], true_betas))
}

############################################
# W <- matrix(1/C, dim(y)[1], C)
# pi <- colMeans(W)
# 
# #W_new <- as.data.frame(W_new)
# #W_new$check <- W_new$V1 + W_new$V2 + W_new$V3
# data <- list(y, X, W)
# 
# all_betas <- theta[[2]]
# all_betas_matrix <- c()
# for (i in all_betas){
#   all_betas_matrix <- c(all_betas_matrix, i)
# }
# theta_vector <- c(as.vector(theta[[1]]), as.vector(all_betas_matrix))
# param <- theta_vector
# 
# objective2(data, theta_vector)
# 
# M <- Mstep2(W, y, X, theta)
# M[[1]] #pi
# M[[2]] #theta
# M[[2]][[1]] #alpha 
# 
# 
# total <- LogL2(theta, pi, y, X)
# W_new <- Estep2(theta, colMeans(W), X)
# 
# #################
# W <- matrix(1/C, dim(y)[1], C)
# J <- 4 
# 
# input_theta <- initial_theta2(J, C)
# M <- Mstep2(W, y, X, input_theta)
# pi <- M[[1]]
# theta <- M[[2]]
# theta[[1]] ######## WRM ALPHAS NIET GOED OP VOLGORDE
# theta[[2]]
# 
# #for (i in 1:ncol(theta[[1]])){
# #  theta[[1]][,i] <- theta[[1]][,1]
# #}
# input_theta <- theta
# theta <- input_to_outputbetas(input_theta)
# theta[[2]]
# 
# W_new <- Estep2_new(theta, colMeans(W), X, y)
# W_newcheck <- as.data.frame(W_new)
# W_newcheck$check <- W_newcheck$V1 + W_newcheck$V2 + W_newcheck$V3
# colMeans(W_newcheck)

