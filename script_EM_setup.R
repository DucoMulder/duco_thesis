.libPaths("C:/Users/ducom/Documents/R/win-library/4.0")

install.packages("numDeriv")
install.packages("matlib")

library("numDeriv")
library("matlib")

LogL <- function(theta, pi, y, X) {
  pi <- matrix(pi)
  theta_matrix <- matrix(theta, nrow = length(pi), ncol = dim(X)[2], byrow = TRUE)
  sum_2 <- 0
  for (i in 1:dim(y)[1]){
    for (t in 1:dim(y)[2]) {
      sum_1 <- 0
      for (c in 1:length(pi)) {
        prob1 <- exp(theta_matrix[c,] %*% X[t,])/(1+exp(theta_matrix[c,] %*% X[t,]))
        prob0 <- 1 - prob1
        sum_1 <- sum_1 + pi[c]*(prob1)^y[i, t]*(prob0)^(1-y[i, t]) }
      sum_2 <- sum_2 + log(sum_1)
    }
  }
  return(sum_2)
}

Estep <- function(theta, pi, y, X) {
  theta_matrix <- matrix(theta, nrow = length(pi), ncol = dim(X)[2], byrow = TRUE)
  ccp <- matrix(, dim(y)[1], length(pi))
  for (i in 1:dim(y)[1]) {
    for (c in 1:length(pi)) {
      denom <- 0
      for (k in 1:length(pi)) {
        product_k <- 1
        for (t in 1:dim(y)[2]) {
          prob1 <- exp(theta_matrix[k,] %*% X[t,])/(1+exp(theta_matrix[k,] %*% X[t,]))
          prob0 <- 1 - prob1
          product_k <- product_k * (prob1)^y[i, t]*(prob0)^(1-y[i, t])
        }
        denom <- denom + pi[k] * product_k
      }
      product_c <- 1
      for (t in 1:dim(y)[2]) {
        prob1 <- exp(theta_matrix[c,] %*% X[t,])/(1+exp(theta_matrix[c,] %*% X[t,]))
        prob0 <- 1 - prob1
        product_c <- product_c * (prob1)^y[i, t]*(prob0)^(1-y[i, t])
      }
      pic <- (pi[c] * product_c) / as.numeric(denom)
      ccp[i, c] <- as.numeric(pic)
    }
  }
  return(ccp)
}

Mstep <- function(W, y, X, theta) {
  pi <- colMeans(W)
  data <- list(W, y, X)
  optim <- optim(par = theta, fn = objective, data = data, method = "BFGS")
  theta <- optim$par
  return(list(pi, theta))
}

objective <- function(data, param) {
  total <- 0
  theta_matrix <- matrix(param, nrow = dim(data[[1]])[2], ncol = dim(data[[3]])[2], byrow = TRUE)
  for (i in 1:dim(data[[2]])[1]) {
    for (c in 1:dim(data[[1]])[2]) {
      sum <- 0
      for (t in 1:dim(data[[2]])[2]) {
        prob1 <- exp(theta_matrix[c,] %*% data[[3]][t,])/(1+exp(theta_matrix[c,] %*% data[[3]][t,]))
        prob0 <- 1 - prob1
        sum <- sum + as.numeric(data[[2]][i, t] * log(prob1) + (1-data[[2]][i,t]) * log(prob0))
      }
      total <- total + data[[1]][i, c] * sum
    }
  }
  return(-1*total)
}

EM <- function(K, y, X){
  W <- matrix(1/K, dim(y)[1], K)
  theta <- integer(K*2)
  for (k in 1:K) {
    theta[k*2-1] <- rnorm(1, 2, 0.5)
    theta[k*2] <- rnorm(1, -2, 0.5)
  }
  W_new <- Estep(theta, colMeans(W), y, X)
  iter <- 0
  diff <- 1
  
  while (diff > 0.00001) {
    iter <- iter + 1
    print(iter)
    W <- W_new
    M <- Mstep(W, y, X, theta)
    W_new <- Estep(M[[2]], M[[1]], y, X)
    diff <- 1/(dim(W)[1]*dim(W)[2])*sum((W_new-W)^2)
    print(diff)
  }
  pi <- M[[1]]
  theta <- M[[2]]
  return(list(pi, theta))
}

Estimate <- function(K, y, X) {
  pis <- matrix(, 20, K)
  thetas <- matrix(, 20, K*2)
  LogLs <- integer(20)
  for (i in 1:length(LogLs)) {
    print("Loop")
    print(i)
    EM_out <- EM(K, y, X)
    pis[i,] <- EM_out[[1]]
    thetas[i,] <- EM_out[[2]]
    LogLs[i] <- LogL(EM_out[[2]], EM_out[[1]], y, X)
  }
  maxint <- which.max(LogLs)
  return(list(pis[maxint,], thetas[maxint,]))
}

LogL_gamma <- function(param){
  index <- (length(param)-2)/3
  gamma <- c(param[1:index])
  theta <- c(param[(index+1):length(param)])
  sum_gammas <- sum(exp(gamma))
  pi_gamma <- exp(gamma)/(1+sum_gammas)
  pi_gamma <- append(pi_gamma, 1/(1+sum_gammas))
  LogL_gammas <- LogL(theta, pi_gamma, y, X)
  return(as.numeric(LogL_gammas))
}

Compute_se <- function(param) {
  hes <- hessian(LogL_gamma, param, method="Richardson")
  ses <- sqrt(diag(inv(-1*hes)))
  index <- (length(param)-2)/3
  ses <- ses[(index+1):length(param)]
  return(ses)
}

main <- function(K, y, X) {
  y <- data.matrix(y)
  X <- data.matrix(X)
  opt_pi_theta <- Estimate(K, y, X)
  print("opt_pi_theta")
  print(opt_pi_theta)
  pi <- opt_pi_theta[[1]]
  theta <- opt_pi_theta[[2]]
  last_pi <- pi[length(pi)]
  gamma <- log(pi[1:(length(pi))-1])-log(last_pi)
  param_gamma <- append(gamma, theta)
  ses_theta <- Compute_se(param_gamma)
  LogL <- LogL(theta, pi, y, X)
  LogL_gamma <- LogL_gamma(param_gamma)
  return(list(pi, theta, ses_theta, LogL, LogL_gamma))
}

#load Data
setwd('C:/Users/ducom/Documents/AA Thesis/EM_AMM')
rawData <- read.csv("434081.csv") 

X <- unique(rawData["price"])
X <- data.matrix(cbind(rep(1, 25), X))
colnames(X) <- c("constant", "price")
y <- matrix(data.matrix(rawData["y"]), 250, 25)

main_output <- main(2, y, X)
print(main_output)
